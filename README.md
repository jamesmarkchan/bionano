
Little Chromosome Stash

Items to do:
- query abort

Optional:
- export data in desired format
- adjust table column spacing so column data is visible
- prevent duplicate queries
- make chromosomes queries run in parallel

Completed:
- Dataset management (import w status, review, delete, test)
- Query Modes: region-mid, region-range, index
- Single chromosome query
- Multi chromosome query
- parameter persistence
- query history
- query processing status
- query processed in expected order