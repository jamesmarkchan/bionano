
package bionano;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author james
 */
public class SingleWorker extends SwingWorker {
    
    String dataset;
    String chrom;
    long regStart;
    long regEnd;
    Query query;
    Query.Mode mode = Query.Mode.REGION_MID;
    
    public void setMode(Query.Mode mode) {
        this.mode = mode;
    }
    
    public void setDataset(String datasetName) {
        dataset = datasetName;
    }
    
    public void setChromosome(String chromosome) {
        chrom = chromosome;
    }
    
    public void setStart(long start) {
        this.regStart = start;
    }
    
    public void setEnd(long end) {
        this.regEnd = end;
    }
    
    @Override
    protected Object doInBackground() throws Exception {
        query = new Query(chrom);
        query.datasetName = dataset;
        query.mode = mode;
        query.start = regStart;
        query.end = regEnd;
        
        try {
            query.execute();
        } catch (RuntimeException re) {
            Logger.getLogger(SingleWorker.class.getName()).log(Level.SEVERE, null, re);
        }
        return query;
    }
    
    @Override
    protected void done() {
        
        BioNano.query = query;
        BioNano.frame.stopTimer();
        BioNano.frame.refresh();
        
        DefaultTableModel model = (DefaultTableModel) BioNano.frame.getResultsTable().getModel();
        model.setRowCount(0);
        query.results.forEach((r) -> {
            model.addRow(new Object[]{r.id, r.chrom, r.mid, r.start, r.end, r.value});
        });
        

        
    }
    
}
