
package bionano;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author james
 */
public class ImportStats {
    String datasetName = "unset";
    Date startDate = null;
    Date endDate = null;
    String start = "";
    String end = "";
    String duration = "n/a";
    int numChromosomes = 0;
    int numRegions = 0;
    long largestRegion = 0;
    long smallestRegion = 0;
    long avgRegion = 0;
    String chroms = "";
    
    ImportStats(String datasetName) {
        this.datasetName = datasetName;
    }
    
    void start() {
        startDate = new Date(System.currentTimeMillis());
        start = startDate.toString();
    }
    
    void complete() {
        endDate = new Date(System.currentTimeMillis());
        end = endDate.toString();
        long elapsed = endDate.getTime() - startDate.getTime();
        duration = new SimpleDateFormat("mm:ss:SSS").format(new Date(elapsed));
    }
}
