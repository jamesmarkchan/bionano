
package bionano;

/**
 *
 * @author james
 */
public class Region {
    
    /**
     * (8 byte) long id = index +1
     * (8 byte) long mid = (end + start) / 2
     * (8 byte) long regionStart
     * (8 byte) long regionEnd
     * (8 byte) double value
     */    
    public static final int RECORD_SIZE = 8 * 5;
    
    long id;
    String chrom;
    long mid;
    long start;
    long end;
    double value;
    
    Region(String chrosome) {
        chrom = chrosome;
    }
}
