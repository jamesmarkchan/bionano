
package bionano;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author james
 */
public class BioNano {

    public static final String PROPERTIES_FILE_NAME = "import.properties";
    public static final File DATA_ROOT = new File(System.getProperty("user.dir") + File.separator + "data");
    public static final File DEFAULT_IMPORT_FILE = new File(System.getProperty("user.dir") + File.separator + "probes.txt");

    public static LcsJFrame frame;
    
    public static ImportStats stats = null;
    public static TreeSet<String> datasets = new TreeSet<>();
    public static TreeMap<String, ImportStats> datasetStats = new TreeMap<>();
    
    public static File fileToImport = null;
    public static File datasetToImport = null;
    
    public static ImportWorker importWorker = null;
    public static SingleWorker singleWorker = null;
    public static MultiWorker multiWorker = null;
    
    public static Query query = null;
    
    public static LinkedList<String> queryHist = new LinkedList<>();
    
    public static void init() {
        if (DATA_ROOT.exists()) {
            datasets.addAll(Arrays.asList(DATA_ROOT.list()));
        }
        if (DEFAULT_IMPORT_FILE.exists()) {
            fileToImport = DEFAULT_IMPORT_FILE;
        }
    }
    
    public static void saveProperties(ImportStats stats) {
        if (stats == null) {
            System.out.println("invalid stats reference");
            return;
        }
        Properties p = new Properties();
        p.setProperty("start", stats.start);
        p.setProperty("end", stats.end);
        p.setProperty("duration", stats.duration);
        p.setProperty("numChromosomes", String.valueOf(stats.numChromosomes));
        p.setProperty("numRegions", String.valueOf(stats.numRegions));
        p.setProperty("largestRegion", String.valueOf(stats.largestRegion));
        p.setProperty("smallestRegion", String.valueOf(stats.smallestRegion));
        p.setProperty("datasetName", String.valueOf(stats.datasetName));
        p.setProperty("chroms", String.valueOf(stats.chroms));
        File file = new File(BioNano.DATA_ROOT.getAbsolutePath() + File.separator + stats.datasetName + File.separator + PROPERTIES_FILE_NAME);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            p.store(fos, "stats for " + stats.datasetName);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BioNano.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BioNano.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void loadProperties(String datasetName) {
        
        // load from cache if present
        if (datasetStats.containsKey(datasetName)) {
            stats = datasetStats.get(datasetName);
            return;
        }
        
        // load actual properties file
        File file = new File(BioNano.DATA_ROOT.getAbsolutePath() 
                + File.separator + datasetName + File.separator + PROPERTIES_FILE_NAME);
        if (!file.exists()) {
            // TODO: clear properties stats
            stats = null;
            return;
        }
        Properties p = new Properties();
        try (FileInputStream fis = new FileInputStream(file)) {
            p.load(fis);
            stats = new ImportStats(datasetName);
            stats.start = p.getProperty("start");
            stats.end = p.getProperty("end");
            stats.duration = p.getProperty("duration");
            stats.numChromosomes = Integer.valueOf(p.getProperty("numChromosomes"));
            stats.numRegions = Integer.valueOf(p.getProperty("numRegions"));
            stats.largestRegion = Long.valueOf(p.getProperty("largestRegion"));
            stats.smallestRegion = Long.valueOf(p.getProperty("smallestRegion"));
            stats.chroms = p.getProperty("chroms");
            datasets.add(datasetName);
            datasetStats.put(datasetName, stats);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BioNano.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BioNano.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        init();
        
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LcsJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            if (frame == null) {
                frame = new LcsJFrame();
                frame.init();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
        });
    }

    static void deleteDataset(String dataset) {
        
        Path datasetDirToDelete = Paths.get(DATA_ROOT.getAbsolutePath() + File.separator + dataset);
        try {
            Files.walk(datasetDirToDelete)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException ex) {
            Logger.getLogger(BioNano.class.getName()).log(Level.SEVERE, null, ex);
        }
        datasets.remove(dataset);
        datasetStats.remove(dataset);
        frame.refresh();
    }
    
    static public List getChromosomeList(String dataset) {
        File datasetDir = new File(BioNano.DATA_ROOT.getAbsolutePath() + File.separator + dataset);
        TreeMap<Integer, String> numChromMap = new TreeMap<>();
        TreeMap<String, String> alphaChromMap = new TreeMap<>();
        ArrayList<String> finalList = new ArrayList();
        for (String fName : datasetDir.list()) {
            if (fName.startsWith("chr") && fName.length() >= 4) {
                String cId = fName.substring(3, fName.length());
                if (cId.matches("\\d+")) {
                    Integer cIdInt = Integer.valueOf(cId);
                    numChromMap.put(cIdInt, fName);
                } else {
                    alphaChromMap.put(cId, fName);
                }
            }
        }
        
        numChromMap.keySet().forEach((i) -> {
            finalList.add(numChromMap.get(i));
        });
        alphaChromMap.keySet().forEach((s) -> {
            finalList.add(alphaChromMap.get(s));
        });
        
        System.out.println("Chromosome processing sequence:" + finalList);
        return finalList;
    }
}
