
package bionano;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;

/**
 * @author james
 */
public class MultiWorker extends SwingWorker <Object, Object> {
    
    String dataset;
    String startChrom;
    long startRegion;
    String endChrom;
    long endRegion;
    
    ArrayList<String> interChroms = new ArrayList<>();
    
    Query rootQuery = new Query();
    Query startQuery;
    ArrayList<Query> interQueries = new ArrayList();
    Query endQuery;
    Query.Mode mode = Query.Mode.REGION_MID;
    
    public void setMode(Query.Mode mode) {
        this.mode = mode;
    }
    public void setDataset(String dataset) {
        this.dataset = dataset;
    }
    public void setStartChrom(String chrom) {
        startChrom = chrom;
    }
    public void setStartRegion(long region) {
        startRegion = region;
    }
    public void setEndChrom(String chrom) {
        endChrom = chrom;
    }
    public void setEndRegion(long region) {
        endRegion = region;
    }

    @Override
    protected Object doInBackground() throws Exception {
        
        BioNano.query = rootQuery;
        
        rootQuery.startTime = System.currentTimeMillis();
        
        System.out.println("EXECUTING MULTI WORKER");
        startQuery = new Query(startChrom);
        startQuery.datasetName = dataset;
        startQuery.mode = mode;
        startQuery.start = startRegion;
        startQuery.end = Long.MAX_VALUE;
        rootQuery.addSubQuery(startQuery);
        
        List chroms = BioNano.getChromosomeList(dataset);
        int startIndex = chroms.indexOf(startChrom);
        int endIndex = chroms.indexOf(endChrom);
        interChroms.addAll(chroms.subList(startIndex + 1, endIndex));
        System.out.println("sInx = " +startIndex + " endIdx=" + endIndex+ " interchromas: "+ interChroms);
        
        interChroms.forEach((ic) -> {
            Query iQuery = new Query(ic);
            iQuery.datasetName = dataset;
            iQuery.mode = mode;
            iQuery.start = 0;
            iQuery.end = Long.MAX_VALUE;
            interQueries.add(iQuery);
            rootQuery.addSubQuery(iQuery);
        });
        
        endQuery = new Query(endChrom);
        endQuery.datasetName = dataset;
        endQuery.mode = mode;
        endQuery.start = 0;
        endQuery.end = endRegion;
        rootQuery.addSubQuery(endQuery);
        
        BioNano.query = rootQuery;
        
        try {
            
            publish("Processing " + startQuery.chrom);
            startQuery.execute();
            rootQuery.results.addAll(startQuery.results);
            publish("Completed " + startQuery.chrom);
            
            interQueries.forEach((iQuery) -> {
                publish("Processing " + iQuery.chrom);
                iQuery.useDistance = false;
                iQuery.execute();
                rootQuery.results.addAll(iQuery.results);
                publish("Completed " + iQuery.chrom);
            });
            
            publish("Processing " + endQuery.chrom);
            endQuery.execute();
            rootQuery.results.addAll(endQuery.results);
            publish("Completed " + endQuery.chrom);
        } catch (RuntimeException re) {
            Logger.getLogger(SingleWorker.class.getName()).log(Level.SEVERE, null, re);
        }
        return rootQuery;
    }
    
    @Override
    protected void process(List<Object> chunks) {
        chunks.forEach((Object o) -> {
            if (o instanceof String) {
                String message = o.toString();
                BioNano.query.status = message;
            }
        });
    }
    
    @Override
    protected void done() {
        System.out.println("MULTI WORKER COMPLETE");
        BioNano.query = rootQuery;
        BioNano.frame.stopTimer();
        BioNano.frame.refresh();
        
        DefaultTableModel model = (DefaultTableModel) BioNano.frame.getResultsTable().getModel();
        model.setRowCount(0);
        rootQuery.results.forEach((r) -> {
            model.addRow(new Object[]{r.id, r.chrom, r.mid, r.start, r.end, r.value});
        });
    }
}
