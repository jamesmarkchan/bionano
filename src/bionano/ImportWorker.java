
package bionano;

import static bionano.BioNano.frame;
import static bionano.BioNano.saveProperties;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker;

/**
 *
 * @author james
 */
public class ImportWorker extends SwingWorker<ImportStats, ImportStats> {

    File datasetToImport;
    File fileToImport;
    boolean limitImport;
    long importLimit = 0;
    
    public void setLimitImport(boolean enable) {
        limitImport = enable;
    }
    
    public void setImportLimit(long limit) {
        importLimit = limit;
    }
    
    public void setDatasetDir(File datasetDir) {
        datasetToImport = datasetDir;
    }
    
    public void setFileToImport(File importFile) {
        fileToImport = importFile;
    }
    
    @Override
    protected ImportStats doInBackground() throws Exception {
        ImportStats stats = doImport(fileToImport, datasetToImport);
        return stats;
    }
        
    @Override
    protected void process(List<ImportStats> chunks) {
        if (!chunks.isEmpty()) {
            BioNano.stats = chunks.get(0);
        }
        frame.updateImportStats();
    }
    
    @Override
    protected void done() {
        frame.refresh();
        BioNano.importWorker = null;
    }

    /**
     * @param datasetDir
     * @param fileToImport
     * @return 
     */
    public ImportStats doImport(File fileToImport, File datasetDir) {
        
        ImportStats stats = new ImportStats(datasetDir.getName());
        BioNano.stats = stats;
        stats.start();
        
        Scanner scanner = null;
        
        if (fileToImport == null) {
            System.out.println("file is not present: aborting");
            return stats;
        }
        try {
            scanner = new Scanner(fileToImport);
            scanner.useDelimiter("\n");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BioNano.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if (scanner == null) {
            System.out.println("no file " + fileToImport.getName());
            return stats;
        }
        
        // track the number of chromosomes
        Map <String, Long> cCountMap = new HashMap<>();
        Map <String, File> cFileMap = new HashMap<>();
                    
        // read off header
        scanner.next();
        
        for (int i = 0 ; scanner.hasNext() && !isCancelled(); i++) {
        
            if (limitImport && i>=importLimit) {
                System.out.println("Import limit of " + importLimit + " reached.");
                break;
            }
        
            String next = scanner.next();
            String [] values = next.split("\t");
            String chrom = values[0];
            long start = Long.valueOf(values[1]);
            long end = Long.valueOf(values[2]);
            long regionSize = end - start;
            long mid = (end + start) / 2;
            float value = Float.valueOf(values[3]);
            File chromFile;
            Long count;
            if (cFileMap.containsKey(chrom)) {
                // add to existing chromisome db file
                chromFile = cFileMap.get(chrom);
                count = cCountMap.get(chrom);
            } else {
                // create chromosome db file
                chromFile = new File(datasetDir.getAbsolutePath() + File.separator + chrom);
                count = new Long(0);
                cFileMap.put(chrom, chromFile);
                cCountMap.put(chrom, count);
                stats.numChromosomes++;
            }
            
            if (stats.largestRegion < regionSize) {
                stats.largestRegion = regionSize;
            }
            stats.numRegions++;
            count++;
            cCountMap.put(chrom, count);
            
            try (FileOutputStream output = new FileOutputStream(chromFile, true)) {
                output.write(ByteBuffer.wrap(new byte[8]).putLong(count).array());
                output.write(ByteBuffer.wrap(new byte[8]).putLong(mid).array());
                output.write(ByteBuffer.wrap(new byte[8]).putLong(start).array());
                output.write(ByteBuffer.wrap(new byte[8]).putLong(end).array());
                output.write(ByteBuffer.wrap(new byte[8]).putDouble(value).array());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(BioNano.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(BioNano.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if (i % 100 == 0) {
                publish(stats);
            }
            
            System.out.println(next);
        }
        scanner.close();
        stats.complete();
        stats.chroms = cCountMap.toString();
        saveProperties(stats);
        
        return stats;
    }
}
