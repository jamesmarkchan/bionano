
package bionano;

import static bionano.Region.RECORD_SIZE;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author james
 */
public class Query {
    
    /**
     * Defined Modes
     */
    public enum Mode {
        /** 
         * Proposed solution: search start index by binary search on mid point 
         * and then increase range by search distance (1/2 max distance).
         * Applied review feedback from bionano team.
         */
        REGION_MID,
        /**
         * Presented implementation: binary search on start and end columns
         * then bc of dataset search forwards and backwards for duplicates.
         */
        REGION_RANGE,
        /**
         * Diagnostic mode for reviewing dataset.
         */
        INDEX
    }
    
    /** dataset name, same as directory containing dataset. */
    String datasetName;
    
    /** chromosome name. */
    String chrom = "";
    
    /** query mode. */
    Mode mode = Mode.REGION_MID;
    
    /** File backing this Query's target. */
    File file = null;
    
    /** Start timestamp. */
    long startTime = 0;
    
    /** End timestamp. */
    long endTime = 0;
    
    /** String displaying elapsed duration of the query. */
    String elapsed = "";
    
    /** count of all regions processed */
    int processedRegions = 0;
    
    /** start region or start index */
    long start;
    
    /** end region or index. */
    long end;
    
    /** a status string of this query, might not be used for child queries. */
    String status;
    
    /** use for non intermediate queries */
    boolean useDistance = true;
    
    /** used by multi query worker */
    ArrayList<Query> subQueries = new ArrayList<>();
    
    /** query results */
    ArrayList<Region> results = new ArrayList<>();
    
    Query() {
    }
    
    Query(String chromosome) {
        chrom = chromosome;
    }
    
    public long getProcessed() {
        long total = processedRegions;
        for (Query q : subQueries) {
            total += q.processedRegions ;
        }
        return total;
    }
    
    public void addSubQuery(Query query) {
        subQueries.add(query);
    }
    
    public void execute() {
        System.out.println("EXECUTING IN " + mode + " MODE");
        
        // open the corresponding chromosome file
        File cFile = getFile();
        if (!cFile.exists()) {
            System.out.println("chromosome file does not exist " + cFile.getAbsolutePath());
            return;
        }
        
        startTime = System.currentTimeMillis();
        switch (mode) {
            case REGION_RANGE:
                // use bs on start and end to find endpoints
                executeRegionByRangeQuery();
                break;
            case REGION_MID:
                // use bs on mid - half length distance
                executeRegionByMidQuery();
                break;
            case INDEX:
                executeIndexQuery();
                break;
        }
        endTime = System.currentTimeMillis();
        long elapsedMs = endTime - startTime;
        elapsed = new SimpleDateFormat("mm:ss:SSS").format(new Date(elapsedMs));
        System.out.println("Completed " + mode + " query in " + elapsed);
    }
    
    public void updateElapsed() {
        long elapsedMs = System.currentTimeMillis() - startTime;
        elapsed = new SimpleDateFormat("mm:ss:SSS").format(new Date(elapsedMs));
    }
    
    public void executeRegionByRangeQuery() {
        // size of the file
        long fSize = getFile().length();
        long numRecords = fSize / RECORD_SIZE;
        long startId = 1;
        long endId = numRecords;

        // read the mid record
        long targetId = Math.max(0, (startId + endId) / 2);
        Region targetRegion = getRegion(targetId);
        long lastVisitedId;
        Region startRegion = null;
        Region endRegion = null;
        
        // search for the start index
        while (startRegion == null) {
            if (start < targetRegion.start) {
                // seach between start id and target id
                endId = targetId;
                targetId = Math.max(startId, (startId + targetRegion.id) / 2);
            } else if (targetRegion.start < start) {
                // search between target and end
                startId = targetId;
                targetId = Math.min(endId, (targetRegion.id + endId) / 2);
            }
            lastVisitedId = targetRegion.id;
            targetRegion = getRegion(targetId);
            System.out.println("traversing for start id=" + targetId + " s-region=" + targetRegion.start);
            if (targetRegion.start == start || lastVisitedId == targetRegion.id) {
                startRegion = targetRegion;
            }
        }
        // search for duplicate start
        while (targetRegion.id > 1) {
            Region prevRegion = getRegion(targetRegion.id-1);
            // previous is the same or overlap
            if (prevRegion.start == targetRegion.start || prevRegion.end >= start) {
                System.out.println("backing up to " + prevRegion.id);
                targetRegion = prevRegion;
            } else {
                startRegion = targetRegion;
                break;
            }
        }
        
        System.out.println("START id=" + startRegion.id);
        
        // start of range found adjust index upwards to include any record withing range.
        startId = targetId + 1;
        endId = numRecords;
        targetId = (startId + endId) / 2;
        targetRegion = getRegion(targetId);
        while (endRegion == null) {
            if (targetRegion.end < end) {
                // search between target and end
                startId = targetId;
                targetId = Math.min(endId, (targetRegion.id + endId) / 2);
            } else if (end < targetRegion.end) {
                // seach between start id and target id
                endId = targetId;
                targetId = Math.max(startId, (startId + targetRegion.id) / 2);
            }
            lastVisitedId = targetRegion.id;
            targetRegion = getRegion(targetId);
            System.out.println("traversing for end id=" + targetId + " e-region=" + targetRegion.end);
            if (targetRegion.end == end || lastVisitedId == targetRegion.id) {
                endRegion = targetRegion;
            }
        }
        
        // search for duplicate or overlap
        while (targetRegion.id < numRecords) {
            Region nextRegion = getRegion(targetRegion.id+1);
            // next is the same or overlap
            if (nextRegion.end == targetRegion.end || nextRegion.start <= end) {
                System.out.println("edging forward to " + nextRegion.id);
                targetRegion = nextRegion;
            } else {
                endRegion = targetRegion;
                break;
            }
        }
        
        System.out.println("END id=" + endRegion.id);
        
        try (RandomAccessFile raf = new RandomAccessFile(getFile(), "r")) {
        
            for (long r = startRegion.id - 1; r < endRegion.id; r++) {
                long offset = r * RECORD_SIZE;
                byte[] data = new byte[RECORD_SIZE];
                raf.seek(offset);
                raf.readFully(data);
                Region region = new Region(chrom);
                region.id = ByteBuffer.wrap(Arrays.copyOfRange(data, 0, 7 + 1)).getLong();
                region.mid = ByteBuffer.wrap(Arrays.copyOfRange(data, 8, 15 + 1)).getLong();
                region.start = ByteBuffer.wrap(Arrays.copyOfRange(data, 16, 23 + 1)).getLong();
                region.end = ByteBuffer.wrap(Arrays.copyOfRange(data, 24, 31 + 1)).getLong();
                region.value = ByteBuffer.wrap(Arrays.copyOfRange(data, 32, 39 + 1)).getDouble();
                if (start <= region.end && region.end <= end || start <= region.start && region.start <= end) {
                    results.add(region);
                    processedRegions++;
                } else {
                    System.out.println("skipping out of bounds s=" + region.start + " e=" + region.end);
                }
            }
        
        } catch (IOException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void executeRegionByMidQuery() {
        // size of the file
        long fSize = getFile().length();
        long numRecords = fSize / RECORD_SIZE;
        long startId = 1;
        long endId = numRecords;

        // get max region size for dataset / calculate half
        File dspFile = new File(BioNano.DATA_ROOT.getAbsolutePath() 
                + File.separator + datasetName + File.separator + "import.properties");
        long searchDistance = 0;
        try (FileInputStream inputStream = new FileInputStream(dspFile)) {
            Properties dataProperties = new Properties();
            dataProperties.load(inputStream);
            String largestRegionString = dataProperties.getProperty("largestRegion");
            searchDistance = Long.valueOf(largestRegionString) / 2;
            // pad distance by one
            searchDistance++;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("searchDistance=" + searchDistance);
        
        // read the mid record
        long targetId = Math.max(0, (startId + endId) / 2);
        Region targetRegion = getRegion(targetId);
        long lastVisitedId;
        long targetStartMid = useDistance ? start - searchDistance : start;
        Region startRegion = null;
        
        // search for the start region
        while (startRegion == null) {
            if (targetStartMid < targetRegion.mid) {
                // seach between start id and target id
                endId = targetId;
                targetId = Math.max(startId, (startId + targetRegion.id) / 2);
            } else if (targetRegion.mid < targetStartMid) {
                // search between target and end
                startId = targetId;
                targetId = Math.min(endId, (targetRegion.id + endId) / 2);
            }
            lastVisitedId = targetRegion.id;
            targetRegion = getRegion(targetId);
            System.out.println("traversing for start id=" + targetId + " start-region-mid=" + targetRegion.mid);
            if (lastVisitedId == targetRegion.id) {
                startRegion = targetRegion;
            }
        }
        
        // start of range found adjust index upwards to include any record withing range.
        // search for duplicate start
        while (targetRegion.id > 1) {
            Region prevRegion = getRegion(targetRegion.id-1);
            // previous is the same or overlap
            if (prevRegion.start == targetRegion.start || prevRegion.end >= start) {
                System.out.println("backing up to " + prevRegion.id);
                targetRegion = prevRegion;
            } else {
                startRegion = targetRegion;
                break;
            }
        }
        
        System.out.println("START id=" + startRegion.id);
        
        try (RandomAccessFile raf = new RandomAccessFile(getFile(), "r")) {
        
            long endRange = useDistance ? end + searchDistance : end;
            // quick overflow check (we shouldn't have negatives)
            if (endRange <= 0) {
                endRange = end;
            }
            boolean pastMidEndRegion = false;
            for (long r = startRegion.id - 1; r < numRecords && !pastMidEndRegion; r++) {

                long offset = r * RECORD_SIZE;
                byte[] data = new byte[RECORD_SIZE];
                raf.seek(offset);
                raf.readFully(data);
                Region region = new Region(chrom);
                region.id = ByteBuffer.wrap(Arrays.copyOfRange(data, 0, 7 + 1)).getLong();
                region.mid = ByteBuffer.wrap(Arrays.copyOfRange(data, 8, 15 + 1)).getLong();
                region.start = ByteBuffer.wrap(Arrays.copyOfRange(data, 16, 23 + 1)).getLong();
                region.end = ByteBuffer.wrap(Arrays.copyOfRange(data, 24, 31 + 1)).getLong();
                region.value = ByteBuffer.wrap(Arrays.copyOfRange(data, 32, 39 + 1)).getDouble();
                boolean regStartWithinQueryRange = start <= region.start && region.start <= end;
                boolean regEndWithinQueryRange = start <= region.end && region.end <= end;
                if (regStartWithinQueryRange || regEndWithinQueryRange) {
                    results.add(region);
                    processedRegions++;
                } else {
                    System.out.println("skipping out of bounds s=" + region.start + " e=" + region.end);
                }
                
                if (region.mid > endRange) {
                    System.out.println("region mid is past end + search distance (if no overflow)");
                    //System.out.println("reg.mid=" + region.mid + " end=" + end + " searchDist="+searchDistance);
                    pastMidEndRegion = true;
                }
            }
        
        } catch (IOException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void executeIndexQuery() {
        
        long fSize = getFile().length();
        long numRecords = fSize / RECORD_SIZE;
        long maxIdx = numRecords - 1;
        long startIdx = Math.max(0, start - 1);
        long endIdx = Math.min(maxIdx, end - 1);
        
        try (RandomAccessFile raf = new RandomAccessFile(getFile(), "r")) {
            for (long r = startIdx; r < endIdx; r++) {
                long offset = r * RECORD_SIZE;
                byte[] data = new byte[RECORD_SIZE];
                raf.seek(offset);
                raf.readFully(data);
                Region region = new Region(chrom);
                region.id = ByteBuffer.wrap(Arrays.copyOfRange(data, 0, 7 + 1)).getLong();
                region.mid = ByteBuffer.wrap(Arrays.copyOfRange(data, 8, 15 + 1)).getLong();
                region.start = ByteBuffer.wrap(Arrays.copyOfRange(data, 16, 23 + 1)).getLong();
                region.end = ByteBuffer.wrap(Arrays.copyOfRange(data, 24, 31 + 1)).getLong();
                region.value = ByteBuffer.wrap(Arrays.copyOfRange(data, 32, 39 + 1)).getDouble();
                if (start <= region.id && region.id <= end) {
                    results.add(region);
                    processedRegions++;
                } else {
                    System.out.println("removing out of bounds s=" + region.start + " e=" + region.end);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public File getFile() {
        if (file == null) {
            file = new File(BioNano.DATA_ROOT.getAbsolutePath() + File.separator 
                + datasetName + File.separator + chrom);
        }
        return file;
    }
    
    public Region getRegion(long rId) {
        long index = rId - 1;
        long offset = index * RECORD_SIZE;
        byte[] data = new byte[RECORD_SIZE];
        try (RandomAccessFile raf = new RandomAccessFile(getFile(), "r")) {
            raf.seek(offset);
            raf.readFully(data);
        } catch (IOException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        }

        Region region = new Region(chrom);
        region.id = ByteBuffer.wrap(Arrays.copyOfRange(data, 0, 7 + 1)).getLong();
        region.mid = ByteBuffer.wrap(Arrays.copyOfRange(data, 8, 15 + 1)).getLong();
        region.start = ByteBuffer.wrap(Arrays.copyOfRange(data, 16, 23 + 1)).getLong();
        region.end = ByteBuffer.wrap(Arrays.copyOfRange(data, 24, 31 + 1)).getLong();
        region.value = ByteBuffer.wrap(Arrays.copyOfRange(data, 32, 39 + 1)).getDouble();

        return region;
    }
}
